<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use KDA\Laravel\Layouts\Facades\LayoutManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        LayoutManager::registerRenderHook(
            'head.styles',
            function ($context) {
                return Blade::render("@vite('resources/css/app.css')");
            }
        );

        LayoutManager::registerRenderHook(
            'head.scripts',
            function ($context) {
                 return Blade::render("@vite('resources/js/app.js')");
            }
        );
    }
}
