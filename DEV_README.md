# KDA dev Readme

## Bootstrap env standalone 
#### aka dev with no package debug
    
    ln -s docker-compose.dev.yml docker-compose.yml
    ln -s composer.prod.json composer.json

## Bootstrap env monorepo
#### aka dev with package debug

change `KDA_MONOREPO_ROOT_PATH` in env

    ln -s docker-compose.dev.yml docker-compose.yml
    ln -s composer.monorepo.json composer.json


## Project ignition (install project on computer) 


clone the repository from gitlab

```bash

    ln -s docker-compose.dev.yml docker-compose.yml
    ln -s composer.prod.json composer.json
    php artisan composer install
    sail up -d
    sail composer update
    sail  artisan vendor:publish --provider="KDA\Laravel\Layouts\ServiceProvider"  --tag="assets" --force
    yarn 
    yarn dev --host
```

## Setup Filament

```bash

composer require filament/filament:"^3.0-stable"


```
