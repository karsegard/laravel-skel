.PHONY: helpers
helpers:
	php artisan ide-helper:generate
	php artisan ide-helper:models -F idehelpers/ModelHelpers.php -M
.PHONY: helpers-sail
helpers-sail:
	vendor/bin/sail artisan ide-helper:generate
	vendor/bin/sail artisan ide-helper:models -F idehelpers/ModelHelpers.php -M
.PHONY: analyse
analyse:
	./vendor/bin/phpstan analyse --xdebug
.PHONY: initkda
initkda:
	$(MAKE) clean-env
	ln -s composer.prod.json composer.json 
	composer install --ignore-platform-reqs
.PHONY: monorepo
monorepo:
	$(MAKE) initkda
	$(MAKE) clean-env
	chmod ugo+rwx ./composer || echo "folder composer cannot be created"
	ln -s composer.monorepo.json composer.json 
	ln -s docker-compose.dev.yml docker-compose.yml
	vendor/bin/sail up -d
	vendor/bin/sail composer update
.PHONY: clean-env
clean-env:
	rm composer.json || echo "no composer.json";
	rm docker-compose.yml || echo "no docker-compose";
	rm composer.lock || echo "no composer.lock";
.PHONY: clean
clean:
	$(MAKE) clean-env
	rm -rf vendor || echo "no vendor directory";
.PHONY: composer
composer:
	../../clis/monorep-cli/kdacli composer:sync monorepo prod --diff-only