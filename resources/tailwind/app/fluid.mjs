const minScreenSize= '320px';
const maxScreenSize= '1920px';

const screenBoundary = {minvw:minScreenSize,maxvw:maxScreenSize}

/* definition des typo fluides

    génere les classes text-[BREAKPOINT]-fluid et leading-[BREAKPOINT]-fluid
    
    ne pas oublier d'ajouter la contrepartie dans resources/css/app.css
    .typo-BREAKPOINT_NAME {
        @apply text-title-fluid leading-title-fluid ;
    }



*/
const textSizes = {

    xs: { // 20px
        min: '10.24px', // 0.75rem
        max: '10.24px', // 0.875rem
        ...screenBoundary
    },
    sm: { // 20px
        min: '12.8px', // 0.75rem
        max: '12.8px', // 0.875rem
        ...screenBoundary
    },

    md: { //26px
        min: '16px', //
        max: '16px', //
        ...screenBoundary
    },
    lg: { //26px
        min: '20px', //
        max: '20px', //
        ...screenBoundary
    },
    xl: { //50px
        min: '25px', //
        max: '25px', //
        ...screenBoundary
    },

    '2xl':{
        min:'31.25px',
        max:'31.25px',
    },
    '3xl':{
        min:'39.06px',
        max:'39.06px',
    },
   
}

const leading = {
    xs: {
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
    sm: {
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
    'input-sm':{
        sm: {
            min: '1.2em',
            max: '1.2em',
            ...screenBoundary
        },
    },
    md: {
        min: '1.25em',
        max: '1.25em',
        ...screenBoundary
    },
    'input-md': {
        min: '1.5em',
        max: '1.5em',
        ...screenBoundary
    },
    lg: {
        min: '1.25em',
        max: '1.25em',
        ...screenBoundary
    },
    xl: {
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
    '2xl': {
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
    '3xl': {
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
    title:{
        min: '1.2em',
        max: '1.2em',
        ...screenBoundary
    },
}

module.exports = {
    textSizes,leading
}