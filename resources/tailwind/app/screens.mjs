module.exports = {
    'lgcy': '320px', //phone
    'old': '480px',
    'sm': '640px',
    'md': '768px', //tablet
    'lg': '1024px', //desktop
    'xl': '1280px', 
    '2xl': '1536px',
    '3xl': '1920px',

}