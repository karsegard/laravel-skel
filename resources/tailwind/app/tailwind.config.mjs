import __extends  from './extends.mjs';
import __screens  from './screens.mjs';
import __fluid  from './fluid.mjs';
import __fontFamilies  from  './font-families.mjs';
import __colors  from  './colors.mjs';
import __theme  from  './theme.mjs';
import plugin from 'tailwindcss/plugin';
import fluidPlugin from './fluid-plugin';
import formsPlugin from '@tailwindcss/forms';
import typoPlugin from '@tailwindcss/typography';
import layouts_preset from '../../../vendor/kda/laravel-layouts/resources/tailwind/tailwind.config.preset.mjs'

module.exports = {
    presets: [
        layouts_preset,
    ],

    content: [
        "./resources/views/**/*.blade.php",
        "./vendor/kda/laravel-layouts/resources/views/**/*.blade.php",
        "./vendor/kda/livewire-modal/resources/views/*.blade.php",
    ],
    darkMode: 'class',
    safelist: [
        {
           pattern: /max-w-(sm|md|lg|xl|2xl|3xl|4xl|5xl|6xl|7xl)/,
           variants: ['sm', 'md', 'lg', 'xl', '2xl'],
        }
     ],
    textSizes: {
        ...__fluid.textSizes
    },

    leading: {
        ...__fluid.leading

    },
    margin:{
        ...__fluid.leading
    },

    theme: {
        ...__theme,

        screens: __screens,


        extend: {
            fontFamily: {
                ...__fontFamilies
            },
            colors: {
                ...__colors
            },
            ...__extends
        },
    },
    plugins: [
        fluidPlugin({
            textSizes: true,
            leading: true,
            margin:true
        }),
        formsPlugin(),
        typoPlugin
    ],
}
