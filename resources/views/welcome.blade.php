<x-layouts.base>
    <div class="typo-xl">test</div>
    <div class="typo-md">test</div>
    <div class="typo-sm">test</div>
    <div class="bg-danger-300">
        <div>This ugly font is here to show everything is allright</div>
        <div class="font-primary">
            <div class="typo-xl">Hello world</div>
            <div class="typo-2xl">Hello world</div>
            <div class="typo-3xl">Hello world</div>
        </div>
    </div>
</x-layouts.base>
